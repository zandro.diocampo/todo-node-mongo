
const express = require('express');
const morgan = require('morgan');

const app = express();


app.use(express.json());
app.use(morgan('dev'));


let todos = [];

// GET /todos
app.get('/todos', (req, res) => {
  res.status(200).json(todos);
});

// GET /todos/:id
app.get('/todos/:id', (req, res) => {
  const todoId = req.params.id;
  const todo = todos.find(todo => todo.id === parseInt(todoId));
  if (todo) {
    res.status(200).json(todo);
  } else {
    res.status(404).json({ error: 'Todo not found' });
  }
});

// POST /todos
app.post('/todos', (req, res) => {
  const { task } = req.body;
  if (task) {
    const newTodo = {
      id: todos.length + 1,
      task: task,
      checked: false,
    };
    todos.push(newTodo);
    res.status(201).json(newTodo);
  } else {
    res.status(400).json({ error: 'Task property is required' });
  }
});

// PATCH /todos/:id
app.patch('/todos/:id', (req, res) => {
  const todoId = req.params.id;
  const { checked } = req.body;
  const todo = todos.find(todo => todo.id === parseInt(todoId));
  if (todo) {
    if (checked !== undefined) {
      todo.checked = checked;
      res.status(200).json(todo);
    } else {
      res.status(400).json({ error: 'Checked property should be a boolean' });
    }
  } else {
    res.status(404).json({ error: 'Todo not found' });
  }
});

// DELETE /todos/:id
app.delete('/todos/:id', (req, res) => {
  const todoId = req.params.id;
  const todoIndex = todos.findIndex(todo => todo.id === parseInt(todoId));
  if (todoIndex !== -1) {
    todos.splice(todoIndex, 1);
    res.sendStatus(204);
  } else {
    res.status(404).json({ error: 'Todo not found' });
  }
});


const port = 3000;
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
